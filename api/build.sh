#!/bin/bash

get_platform () {
    if [[ "$OSTYPE" == "linux-gnu" ]]; then
        echo 'nix'
    elif [[ "$OSTYPE" == "darwin"* ]]; then
        echo 'nix'
    elif [[ "$OSTYPE" == "cygwin" ]]; then
        echo 'nix'
    elif [[ "$OSTYPE" == "msys" ]]; then
        echo 'windows'
    elif [[ "$OSTYPE" == "win32" ]]; then
        echo 'windows'
    elif [[ "$OSTYPE" == "freebsd"* ]]; then
        echo 'nix'
    else
        echo 'unknown'
        return -1;
    fi

    return 0;
}

get_runtime_configuration () {
    if [[ -z "${ASPNETCORE_ENVIRONMENT}" ]]; then
        echo 'Release'
        return -1;
    fi

    if [[ "${ASPNETCORE_ENVIRONMENT}" == "Local" ]]; then
        echo 'Debug'
    elif [[ "${ASPNETCORE_ENVIRONMENT}" == "Development" ]]; then
        echo 'Debug'
    else
        echo 'Release'
    fi

    return 0;
}

dotnet-format --check --dry-run --verbosity diag

if [[ $( get_platform ) == "nix" ]]; then
    dotnet restore --configfile nuget-nix.config
else
    dotnet restore --configfile nuget.config
fi

dotnet msbuild REPLACE_EXAMPLE_PROJECTNAME.sln -target:Build -property:Configuration=$( get_runtime_configuration ) -property:TreatWarningsAsErrors=true -consoleloggerparameters:Summary -verbosity:minimal -nodeReuse:false -nologo


