#!/bin/bash

usage() { echo "Usage: $0 [-s <string> (solution name)] [-p <string> (project prefix)] [-r <string> (folder name)] [-u <string> (git url)] [-t <string> (git template folder name)] [-l <string> (git dotfiles folder name)] [-g <string> (git group name)]" 1>&2; exit 1; }

while getopts s:p:r:u:t:l:g: option 
do
    case "${option}" 
    in
        s) SLN_NAME=${OPTARG};;
        p) FILE_NAME=${OPTARG};;
        r) GIT_ARTIFACT_FOLDER_NAME=${OPTARG};;
        u) GIT_URL_PREFIX=${OPTARG};;
        t) GIT_TEMPLATE_PREFIX=${OPTARG};;
        l) GIT_DOTFILES_LOCATION=${OPTARG};;
        g) GIT_GROUP_PREFIX=${OPTARG};;
        \?) usage;;
    esac
done

if [[ -z "$CI_PROJECT_DIR" ]]; then
    CI_PROJECT_DIR=$PWD
fi

IIS=$FILE_NAME.IIS
CONSOLE=$FILE_NAME.ConsoleApplication
AZUREFUNCTION=$FILE_NAME.AzureFunction
BOOTSTRAPPER=$FILE_NAME.Bootstrapper
ROUTE=$FILE_NAME.Route
CORE=$FILE_NAME.Core
COMMON=$FILE_NAME.Common
REPOSITORY=$FILE_NAME.Repository
TEST=$FILE_NAME.Test
PUBLIC=$FILE_NAME.Public

IIS_PATH=src/hosts/$IIS
CONSOLE_PATH=src/hosts/$CONSOLE
AZUREFUNCTION_PATH=src/hosts/$AZUREFUNCTION
BOOTSTRAPPER_PATH=src/$BOOTSTRAPPER
ROUTE_PATH=src/$ROUTE
CORE_PATH=src/$CORE
COMMON_PATH=src/$COMMON
REPOSITORY_PATH=src/$REPOSITORY
TEST_PATH=tests/$TEST
PUBLIC_PATH=src/$PUBLIC

WEBAPP_SDK_VERSION=2.2.402
WEBAPP_FRAMEWORK_VERSION=netcoreapp2.2
LIBRARY_FRAMEWORK_VERSION=netstandard2.0

if [ ! -d "$GIT_ARTIFACT_FOLDER_NAME" ]; then
  mkdir $GIT_ARTIFACT_FOLDER_NAME
fi

echo "CLONE FROM REPOSITORY"
git clone $GIT_URL_PREFIX/$GIT_TEMPLATE_PREFIX/dotnet-web.git

cd $GIT_ARTIFACT_FOLDER_NAME

curl --remote-name $GIT_URL_PREFIX/$GIT_TEMPLATE_PREFIX/$GIT_DOTFILES_LOCATION/raw/master/.gitattributes
curl --remote-name $GIT_URL_PREFIX/$GIT_TEMPLATE_PREFIX/$GIT_DOTFILES_LOCATION/raw/master/.gitignore
curl --remote-name $GIT_URL_PREFIX/$GIT_TEMPLATE_PREFIX/$GIT_DOTFILES_LOCATION/raw/master/.dockerignore
curl --remote-name $GIT_URL_PREFIX/$GIT_TEMPLATE_PREFIX/$GIT_DOTFILES_LOCATION/raw/master/gitlab-ci/dotnet/.editorconfig
curl --remote-name $GIT_URL_PREFIX/$GIT_TEMPLATE_PREFIX/$GIT_DOTFILES_LOCATION/raw/master/gitlab-ci/dotnet/.default-ci.yml
curl --remote-name $GIT_URL_PREFIX/$GIT_TEMPLATE_PREFIX/dotnet/raw/master/web/nuget.config
curl --remote-name $GIT_URL_PREFIX/$GIT_TEMPLATE_PREFIX/dotnet/raw/master/web/nuget-nix.config
curl --remote-name $GIT_URL_PREFIX/$GIT_TEMPLATE_PREFIX/dotnet/raw/master/web/build.sh
curl --remote-name $GIT_URL_PREFIX/$GIT_TEMPLATE_PREFIX/dotnet/raw/master/web/Dockerfile
sed -i -e 's#REPLACE_EXAMPLE_PROJECTNAME#'"$SLN_NAME"'#g' .default-ci.yml
sed -i -e 's#REPLACE_EXAMPLE_PROJECTPREFIX#'"$FILE_NAME"'#g' .default-ci.yml
sed -i -e 's#REPLACE_EXAMPLE_PROJECTPREFIX#'"$FILE_NAME"'#g' Dockerfile
sed -i -e 's#REPLACE_EXAMPLE_SPECIFIC_GITLAB_PROJECTNAME#'"$GIT_GROUP_PREFIX/$GIT_TEMPLATE_PREFIX/$GIT_DOTFILES_LOCATION"'#g' .default-ci.yml
sed -i -e 's#REPLACE_EXAMPLE_PROJECTNAME#'"$SLN_NAME"'#g' build.sh

mv .default-ci.yml .gitlab-ci.yml

echo $CI_PROJECT_DIR/$GIT_ARTIFACT_FOLDER_NAME

dotnet new -i $CI_PROJECT_DIR/dotnet-web/src/hosts/DotnetWeb.IIS
dotnet new -i $CI_PROJECT_DIR/dotnet-web/src/hosts/DotnetWeb.ConsoleApplication
dotnet new -i $CI_PROJECT_DIR/dotnet-web/src/hosts/DotnetWeb.AzureFunction
dotnet new -i $CI_PROJECT_DIR/dotnet-web/src/DotnetWeb.Bootstrapper
dotnet new -i $CI_PROJECT_DIR/dotnet-web/src/DotnetWeb.Route
dotnet new -i $CI_PROJECT_DIR/dotnet-web/src/DotnetWeb.Core
dotnet new -i $CI_PROJECT_DIR/dotnet-web/src/DotnetWeb.Common
dotnet new -i $CI_PROJECT_DIR/dotnet-web/src/DotnetWeb.Repository
dotnet new -i $CI_PROJECT_DIR/dotnet-web/tests/DotnetWeb.Test
dotnet new -i $CI_PROJECT_DIR/dotnet-web/src/DotnetWeb.Public

echo "CREATE SOLUTION"
dotnet new sln -n $SLN_NAME

echo "CREATE GLOBALJSON"
dotnet new globaljson --sdk-version $WEBAPP_SDK_VERSION

echo "CREATE NUGET CONFIG"
dotnet new nugetconfig

echo "CREATE PROJECTS"
dotnet new dotnet-web-iis -n $FILE_NAME -o $IIS_PATH
dotnet new dotnet-web-console -n $FILE_NAME  -o $CONSOLE_PATH
dotnet new dotnet-web-function -n $FILE_NAME  -o $AZUREFUNCTION_PATH
dotnet new dotnet-web-bootstrapper -n $FILE_NAME -o $BOOTSTRAPPER_PATH
dotnet new dotnet-web-route -n $FILE_NAME -o $ROUTE_PATH
dotnet new dotnet-web-core -n $FILE_NAME -o $CORE_PATH
dotnet new dotnet-web-common -n $FILE_NAME -o $COMMON_PATH
dotnet new dotnet-web-test -n $FILE_NAME -o $TEST_PATH
dotnet new dotnet-web-repository -n $FILE_NAME -o $REPOSITORY_PATH
dotnet new dotnet-web-public -n $FILE_NAME -o $PUBLIC_PATH

dotnet sln $SLN_NAME.sln add $IIS_PATH/$IIS.csproj $CONSOLE_PATH/$CONSOLE.csproj $AZUREFUNCTION_PATH/$AZUREFUNCTION.csproj $BOOTSTRAPPER_PATH/$BOOTSTRAPPER.csproj $ROUTE_PATH/$ROUTE.csproj $CORE_PATH/$CORE.csproj $COMMON_PATH/$COMMON.csproj $TEST_PATH/$TEST.csproj $REPOSITORY_PATH/$REPOSITORY.csproj $PUBLIC_PATH/$PUBLIC.csproj

echo "REMOVE TEMPLATE FOLDER"
rm -rf ../dotnet-web

echo "BUILD"
# TODO: switch to below build once https://github.com/dotnet/sdk/issues/2902 is merged
# dotnet build $SLN_NAME/$SLN_NAME.sln -c Release -f $WEBAPP_FRAMEWORK_VERSION
dotnet msbuild $SLN_NAME.sln -restore -target:Build -property:Configuration=Release -consoleloggerparameters:Summary -verbosity:minimal -nodeReuse:false -nologo

echo "TEST"
dotnet test $TEST_PATH/$TEST.csproj

echo "PUBLISH"
dotnet publish $IIS_PATH -c Release -f $WEBAPP_FRAMEWORK_VERSION -o $CI_PROJECT_DIR/publish

echo "REMOVE PUBLISH FOLDER"
cd ..
rm -rf publish