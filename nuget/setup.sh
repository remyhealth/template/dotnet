#!/bin/bash

usage() { echo "Usage: $0 [-s <string> (solution name)] [-p <string> (project prefix)] [-r <string> (folder name)] [-u <string> (git url)] [-t <string> (git template folder name)] [-l <string> (git dotfiles folder name)] [-g <string> (git group name)]" 1>&2; exit 1; }

while getopts s:p:r:u:t:l:g: option 
do
    case "${option}" 
    in
        s) SLN_NAME=${OPTARG};;
        p) FILE_NAME=${OPTARG};;
        r) GIT_ARTIFACT_FOLDER_NAME=${OPTARG};;
        u) GIT_URL_PREFIX=${OPTARG};;
        t) GIT_TEMPLATE_PREFIX=${OPTARG};;
        l) GIT_DOTFILES_LOCATION=${OPTARG};;
        g) GIT_GROUP_PREFIX=${OPTARG};;
        \?) usage;;
    esac
done

if [[ -z "$CI_PROJECT_DIR" ]]; then
    CI_PROJECT_DIR=$PWD
fi

CONSOLE=$FILE_NAME.ConsoleApplication
BOOTSTRAPPER=$FILE_NAME.Bootstrapper
COMMON=$FILE_NAME.Common
TEST=$FILE_NAME.Test
SDK=$FILE_NAME.SDK

CONSOLE_PATH=src/hosts/$CONSOLE
BOOTSTRAPPER_PATH=src/$BOOTSTRAPPER
COMMON_PATH=src/$COMMON
TEST_PATH=tests/$TEST
SDK_PATH=src/$SDK

WEBAPP_SDK_VERSION=2.2.402
WEBAPP_FRAMEWORK_VERSION=netcoreapp2.2
LIBRARY_FRAMEWORK_VERSION=netstandard2.0

if [ ! -d "$GIT_ARTIFACT_FOLDER_NAME" ]; then
  mkdir $GIT_ARTIFACT_FOLDER_NAME
fi

echo "CLONE FROM REPOSITORY"
git clone $GIT_URL_PREFIX/$GIT_TEMPLATE_PREFIX/dotnet-nuget.git

cd $GIT_ARTIFACT_FOLDER_NAME

curl --remote-name $GIT_URL_PREFIX/$GIT_TEMPLATE_PREFIX/$GIT_DOTFILES_LOCATION/raw/master/.gitattributes
curl --remote-name $GIT_URL_PREFIX/$GIT_TEMPLATE_PREFIX/$GIT_DOTFILES_LOCATION/raw/master/.gitignore
curl --remote-name $GIT_URL_PREFIX/$GIT_TEMPLATE_PREFIX/$GIT_DOTFILES_LOCATION/raw/master/.dockerignore
curl --remote-name $GIT_URL_PREFIX/$GIT_TEMPLATE_PREFIX/$GIT_DOTFILES_LOCATION/raw/master/gitlab-ci/dotnet/.editorconfig
curl --remote-name $GIT_URL_PREFIX/$GIT_TEMPLATE_PREFIX/$GIT_DOTFILES_LOCATION/raw/master/gitlab-ci/dotnet/.default-ci.yml
curl --remote-name $GIT_URL_PREFIX/$GIT_TEMPLATE_PREFIX/dotnet/raw/master/nuget/nuget.config
curl --remote-name $GIT_URL_PREFIX/$GIT_TEMPLATE_PREFIX/dotnet/raw/master/nuget/nuget-nix.config
curl --remote-name $GIT_URL_PREFIX/$GIT_TEMPLATE_PREFIX/dotnet/raw/master/nuget/build.sh
sed -i -e 's#REPLACE_EXAMPLE_PROJECTNAME#'"$SLN_NAME"'#g' .default-ci.yml
sed -i -e 's#REPLACE_EXAMPLE_PROJECTPREFIX#'"$FILE_NAME"'#g' .default-ci.yml
sed -i -e 's#REPLACE_EXAMPLE_SPECIFIC_GITLAB_PROJECTNAME#'"$GIT_GROUP_PREFIX/$GIT_TEMPLATE_PREFIX/$GIT_DOTFILES_LOCATION"'#g' .default-ci.yml
sed -i -e 's#REPLACE_EXAMPLE_PROJECTNAME#'"$SLN_NAME"'#g' build.sh

mv .default-ci.yml .gitlab-ci.yml

echo $CI_PROJECT_DIR/$GIT_ARTIFACT_FOLDER_NAME

dotnet new -i $CI_PROJECT_DIR/dotnet-nuget/src/hosts/DotnetNuget.ConsoleApplication
dotnet new -i $CI_PROJECT_DIR/dotnet-nuget/src/DotnetNuget.Bootstrapper
dotnet new -i $CI_PROJECT_DIR/dotnet-nuget/src/DotnetNuget.Common
dotnet new -i $CI_PROJECT_DIR/dotnet-nuget/tests/DotnetNuget.Test
dotnet new -i $CI_PROJECT_DIR/dotnet-nuget/src/DotnetNuget.SDK

echo "CREATE SOLUTION"
dotnet new sln -n $SLN_NAME

echo "CREATE GLOBALJSON"
dotnet new globaljson --sdk-version $WEBAPP_SDK_VERSION

echo "CREATE NUGET CONFIG"
dotnet new nugetconfig

echo "CREATE PROJECTS"
dotnet new dotnet-nuget-console -n $FILE_NAME  -o $CONSOLE_PATH
dotnet new dotnet-nuget-bootstrapper -n $FILE_NAME -o $BOOTSTRAPPER_PATH
dotnet new dotnet-nuget-common -n $FILE_NAME -o $COMMON_PATH
dotnet new dotnet-nuget-test -n $FILE_NAME -o $TEST_PATH
dotnet new dotnet-nuget-sdk -n $FILE_NAME -o $SDK_PATH

dotnet sln $SLN_NAME.sln add $CONSOLE_PATH/$CONSOLE.csproj $BOOTSTRAPPER_PATH/$BOOTSTRAPPER.csproj $COMMON_PATH/$COMMON.csproj $TEST_PATH/$TEST.csproj $SDK_PATH/$SDK.csproj

echo "REMOVE TEMPLATE FOLDER"
rm -rf ../dotnet-nuget

echo "BUILD"
# TODO: switch to below build once https://github.com/dotnet/sdk/issues/2902 is merged
# dotnet build $SLN_NAME/$SLN_NAME.sln -c Release -f $WEBAPP_FRAMEWORK_VERSION
dotnet msbuild $SLN_NAME.sln -restore -target:Build -property:Configuration=Release -consoleloggerparameters:Summary -verbosity:minimal -nodeReuse:false -nologo

echo "TEST"
dotnet test $TEST_PATH/$TEST.csproj

echo "PUBLISH"
dotnet publish $CONSOLE_PATH -c Release -f $WEBAPP_FRAMEWORK_VERSION -o $CI_PROJECT_DIR/publish

echo "REMOVE PUBLISH FOLDER"
cd ..
rm -rf publish